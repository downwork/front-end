import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	loginBtnVisible = true;
	signUpVisible = false;

	constructor() { }

	ngOnInit() {
	}

	signUpUser() {
		console.log("Sign Up");
	}

	logInUser() {
		console.log("Log In");
	}

	expandSignUp(event) {
		this.loginBtnVisible = false;
		this.signUpVisible = true;
	}

	hideSignUp(event) {
		this.loginBtnVisible = true;
		this.signUpVisible = false;
	}

}
